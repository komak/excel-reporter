package com.motherinternational.excelreporter;

import io.agroal.pool.DataSource;
import picocli.CommandLine;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import java.util.UUID;

@CommandLine.Command(name = "excelreporter", mixinStandardHelpOptions = true, version = "1.0",
        description = "Prints the checksum (MD5 by default) of a file to STDOUT.")
public class excelreporter implements Runnable {
    @CommandLine.Option(names = {"-n", "--name"}, description = "Who will we greet?", defaultValue = "World")
    String name;

    @Inject
    GreetingService greetingService;


    @Override
    public void run() {
        System.out.println("hhh");greetingService.sayHello(name);
    }
}


